﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour {

	Vector3 startPos;
	bool goRight;
	public float distance;
	Rigidbody2D rb;
	public float speed;

	// Use this for initialization
	void Start () {
		startPos = transform.position;
		rb = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (goRight) {
			rb.MovePosition(new Vector2 (transform.position.x, transform.position.y) + (Vector2.right*Time.deltaTime * speed) );
			if (transform.position.x >= startPos.x + distance) {
				goRight = false;
			}

		} else {
			rb.MovePosition(new Vector2 (transform.position.x, transform.position.y) - (Vector2.right*Time.deltaTime * speed) );
			if (transform.position.x <= startPos.x - distance) {
				goRight = true;
			}
		}
		
	}


}
