﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTrail : MonoBehaviour {

    public int moveSpeed = 230;
    public Vector3 direction;

	void Update () {
        transform.Translate(direction * Time.deltaTime * moveSpeed);
		
	}
}
