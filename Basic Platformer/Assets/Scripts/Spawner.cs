﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] enemies;
    public Vector3 spawnValues;
    public float spawnWait;
    public float spawnMostWait;
    public float spawnLeastWait;
    public int startWait;
    public bool stop;

    //from spawn manager
    public int maxPlatforms = 20;
    //public GameObject platform;
    public float horizontalMin = 6.5f;
    public float horizontalMax = 14f;
    public float verticalMin = -6f;
    public float verticalMax = 6f;
    private Vector2 originPosition;

    int randEnemy;

	void Start () {

        StartCoroutine(waitSpawner());

        //from spawn manager
        originPosition = transform.position;
        //Spawn();

    }
	
	void Update () {

        spawnWait = Random.Range(spawnLeastWait, spawnMostWait);

        //from spawn manager
        for (int i = 0; i < maxPlatforms; i++)
        {

            //Instantiate(platform, randomPosition, Quaternion.identity);

        }

    }

    IEnumerator waitSpawner ()
    {
        yield return new WaitForSeconds(startWait);

        while(!stop)
        {
            randEnemy = 0; //Random.Range(0, 2);
            Vector3 randomPosition = new Vector3(originPosition.x, 0f, -4f) + new Vector3(Random.Range(horizontalMin, horizontalMax), Random.Range(verticalMin, verticalMax),0);
            //Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), 1, Random.Range(-spawnValues.z, spawnValues.z));

            Instantiate(enemies[randEnemy], randomPosition, gameObject.transform.rotation);
            originPosition = randomPosition;
            yield return new WaitForSeconds(spawnWait);
        }
    }
}
